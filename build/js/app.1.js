'use strict';

angular.module("app", ['ngAnimate', 'ngRoute', 'firebase']).config(function ($httpProvider, $locationProvider, $provide, $routeProvider, $sceDelegateProvider) {

  var resolveObj = {

    'currentAuth': function currentAuth(Auth) {
      return Auth.$waitForSignIn();
    }
  };

  $routeProvider.when('/login', {
    templateUrl: 'views/login.html',
    controller: 'LoginController',
    controllerAs: 'ctrl'
  }).when('/calendar', {
    templateUrl: 'views/calendar.html',
    controller: 'CalendarController',
    controllerAs: 'ctrl',
    resolve: resolveObj
  }).when('/recipes', {
    templateUrl: 'views/recipes.html',
    controller: 'RecipesController',
    controllerAs: 'ctrl',
    resolve: resolveObj
  }).when('/my-recipes/:recipeId', {
    templateUrl: 'partials/ricetta.html',
    controller: 'RecipeController',
    controllerAs: 'ctrl',
    resolve: resolveObj
  }).when('/search', {
    templateUrl: 'views/search.html',
    controller: 'SearchController',
    controllerAs: 'ctrl',
    resolve: resolveObj
  }).when('/recipe/:recipeId', {
    templateUrl: 'views/recipe.html',
    controller: 'RecipeController',
    controllerAs: 'ctrl',
    resolve: resolveObj
  }).otherwise({ redirectTo: '/login' });

  $locationProvider.html5Mode({
    enabled: false,
    requireBase: false,
    rewriteLinks: true
  });
})

/* -----------------------------------
  Run method
------------------------------------*/
.run(function ($rootScope, $route, $location, Auth) {

  // https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#authenticating-with-routers
  $rootScope.$on('$routeChangeError', function (event, next, previous, error) {

    if (error === "AUTH_REQUIRED") {
      console.warn('AUTH_REQUIRED');
      $location.path("/login");
    }
  });
});
//# sourceMappingURL=app.1.js.map
