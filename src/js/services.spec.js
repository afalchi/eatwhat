it('should have a title', function() {
  browser.get('http://localhost:3000/#!/login');

  expect(browser.getTitle()).toEqual('EatWhat');


  browser.sleep(5000)
});





xdescribe('Services', function() {

  // Firebase setup
  const config = {
    apiKey: "AIzaSyAMGHvf0fSi3pysWHkOuqc7-0IYY0QlB3M",
    authDomain: "m1sc.firebaseapp.com",
    databaseURL: "https://m1sc.firebaseio.com",
    projectId: "project-5611184154132311931",
    storageBucket: "project-5611184154132311931.appspot.com",
    messagingSenderId: "166751289571"
  };
  firebase.initializeApp(config);

  // Init angular module
  beforeEach( module('app') )
  
  

  // Global vars
  let $controller, 
      $compile,
      $filter,
      $httpBackend, 
      $rootScope
  ;

  // Inject services
  beforeEach(inject( function(
    _$controller_, 
    _$compile_, 
    _$filter_, 
    _$httpBackend_,
    _$rootScope_ 
  ) {
    $controller = _$controller_
    $filter = _$filter_
    $compile = _$compile_
    $httpBackend = _$httpBackend_
    $rootScope = _$rootScope_
  })) 

  
  /* ------------------------------------
    LOGIN
  ------------------------------------ */
  describe('login controller', () => {
    let $scope, controller;

    beforeEach( () => {
      $scope = {}
      controller = $controller('LoginController', { $scope: $scope })
    })

    it('should store State object', () => {
      expect( controller.state ).toBeDefined()
    })

  })


  /* ------------------------------------
    FILTERS
  ------------------------------------ */
  describe('Filters', () => {
    
    describe('urlize', () => {
      it('should convert spaces to dashes', () => {
        const urlize = $filter('urlize')
        expect( urlize('text with spaces') ).toBe('text-with-spaces')
      })
    })
    
    describe('daysAgo', () => {
      it('should return # of days ago', () => {
        const daysAgo = $filter('daysAgo')
        const selectingDate = new Date(2018, 4, 25).getTime()
        const recipeLastDate = new Date(2018, 4, 20).getTime()
        expect( daysAgo(recipeLastDate, selectingDate) ).toBe(5) 
      })
    }) 
  })
  
  
  /* ------------------------------------
    Directives
  ------------------------------------ */
  describe('Directives', () => {

    it('replaces the element with the appropriate content', () => {
      const element = $compile('<a-great-eye></a-great-eye>')($rootScope)
  
      $rootScope.$digest()
  
      expect(element.html()).toContain('lidless, wreathed in flame, 2 times')
    })

  })
  














  // Tryout app
  xdescribe('$scope.grade', () => {
    let $scope, controller;

    beforeEach( () => {
      $scope = {}
      controller = $controller('PasswordController', { $scope: $scope })
    })


    it('sets the strength to "strong" if the password length is >8 chars', () => {
      $scope.password = 'longerthaneightchars'
      $scope.grade()
      expect($scope.strength).toEqual('strong')
    })
    
    it('sets the strength to "weak" if the password length is <=3 chars', () => {
      $scope.password = 'bcd'
      $scope.grade()
      expect($scope.strength).toEqual('weak')
    })

  })




  

});           