angular.module('app')


.controller("RecipesController", function (
  $filter, 
  $location, 
  $log, 
  $scope, 
  $timeout, 
  currentAuth,
  recipesFB, 
  state, 
  today
)  {
  const vm = this;

  vm.mappedRecipesArray = [];
  vm.selectedRecipe = '';
  vm.state = state;
  vm.sortOrder = 'daysAgo';   
  vm.showAddRecipe = false;
  vm.recipesArr = [];
  vm.viewReady = false;


  // Auth check
  if(!currentAuth && state.loginType !== 'withID') {
    $location.url('/login');
    return;
  }

  
  function mapRecipesArray () {
    vm.mappedRecipesArray = vm.recipesArr.map( item => {
      let date = vm.state.selecting.date || today;
      item.daysAgo = $filter('daysAgo')(
        date, $filter('mostRecent')(item.dates)
      );
      return item;
    });
  }


  recipesFB.arr(state.user.uid).$loaded()
    .then(
      data => {
        vm.recipesArr = data;
        mapRecipesArray();
        vm.viewReady = true;
      },
      error => alert('Error loading from Firebase!')    
    )
  ;
   
  // Update week on array update
  recipesFB.arr(state.user.uid).$watch( ev => {
    mapRecipesArray()
  });


  vm.addRecipe = function (ev) {
    ev.preventDefault();
    vm.recipesArr.$add({
      id: vm.newRecipe.toLowerCase().replace(/\s/g, '-'), 
      name: vm.newRecipe,
      dates: []
    });
    vm.newRecipe = '';
    vm.showAddRecipe = false;
  };


  vm.confirmRecipe = function () {    
    vm.state.selecting.selectedRecipe = vm.selectedRecipe;
    vm.state.selecting.active = false;
    recipesFB.ref(state.user.uid).child(`${vm.selectedRecipe}/dates/${state.selecting.date}/${state.selecting.meal}`).set(true);   
    
    $location.url(`/calendar`);
  };


  vm.goBack = function () {
    vm.state.selecting.active = false;
  }
  

  vm.search = function () {
    vm.state.selecting.active = false;
    $location.url('/search');
  }

})