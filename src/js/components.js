angular.module('app')



/* -------------------------------
  Icons
--------------------------------*/
.component('icoCheck', {
  templateUrl: 'components/ico-check.html',
  bindings: {
    selected: '<'
  }
})

.component('icoArrowLeft', {
  templateUrl: 'components/ico-arrow-left.html',
  bindings: {
    color: '@'
  }
})

.component('icoPlus', {
  templateUrl: 'components/ico-plus.html'
})

.component('icoSearch', {
  templateUrl: 'components/ico-search.html'
})




/* -------------------------------
State message
--------------------------------*/
.component('stateMsg', {
  templateUrl: 'components/state-msg.html',
  controller (state) {
    this.state = state;
  }
})


/* -------------------------------
  Remove meal
--------------------------------*/
.component('btnRemove', {
  template: `
    <button 
      href="#!/ricette"      
      ng-click="$ctrl.onSelect()"
    >
      <span class="oi oi-x"></span>
    </button>
  `, 
  bindings: {
    day: '<',
    onSelect: '&'
  }, 
  controller (today) {
    this.today = today;
  }
})


/* -------------------------------
  Toggle selection
--------------------------------*/
.component('toggleSelection', {
  template: `
  <button 
    ng-click="$ctrl.select()" 
    class="recipes__select" 
    title="{{$ctrl.selectedRecipe}} {{$ctrl.recipeId}}"
    ng-class="{'recipes__select--selected': $ctrl.selectedRecipe === $ctrl.recipeId}"
  >
    <ico-check selected="$ctrl.selectedRecipe === $ctrl.recipeId"></ico-check>
  </button>
  `,
  bindings: {
    selectedRecipe: '=',
    recipeId: '<',
  },
  controller () {
    const vm = this;
    vm.select = function () {
      if (vm.selectedRecipe !== vm.recipeId ) {
        vm.selectedRecipe = vm.recipeId;
        $('html, boxy')
          .delay(400)
          .animate({scrollTop: document.body.scrollHeight}, 1500)
        ;
      } else {
        vm.selectedRecipe = '';
      }
    }
  }
})


