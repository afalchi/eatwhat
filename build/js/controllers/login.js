"use strict";

angular.module('app').controller("LoginController", function ($log, $location, $firebaseAuth, $scope, $timeout, state) {

  var vm = this;
  vm.auth = $firebaseAuth();
  vm.user = null;
  vm.logged = false;
  vm.state = state;

  vm.createUser = function () {
    vm.auth.$signInWithPopup("google").then(function (firebaseUser) {
      vm.user = firebaseUser.user;
      vm.state.loginType = 'google';
      vm.state.user = {
        displayName: firebaseUser.user.displayName,
        uid: firebaseUser.user.uid
      };
      vm.logged = true;

      $timeout(function () {
        $location.url('/calendar');
      }, 2000);
    }).catch(function (error) {
      alert("Authentication failed:", error);
    });
  };

  vm.loginWithID = function () {
    vm.state.loginType = 'withID';
    vm.state.user = {
      displayName: 'Guest',
      uid: vm.userUID
    };
    vm.logged = true;
    $timeout(function () {
      $location.url('/calendar');
    }, 2000);
  };

  vm.deleteUser = function () {
    vm.auth.$deleteUser().then(function () {
      vm.message = "User deleted";
    }).catch(function (error) {
      console.error('Error deleting user');
    });
  };
});
//# sourceMappingURL=login.js.map
