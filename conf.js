// conf.js
exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['build/js/services.spec.js'],
  params: {
    url: 'http://www.facebook.com'
  }
}