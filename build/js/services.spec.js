'use strict';

it('should have a title', function () {
  browser.get('http://localhost:3000/#!/login');

  expect(browser.getTitle()).toEqual('EatWhat');

  browser.sleep(5000);
});

xdescribe('Services', function () {

  // Firebase setup
  var config = {
    apiKey: "AIzaSyAMGHvf0fSi3pysWHkOuqc7-0IYY0QlB3M",
    authDomain: "m1sc.firebaseapp.com",
    databaseURL: "https://m1sc.firebaseio.com",
    projectId: "project-5611184154132311931",
    storageBucket: "project-5611184154132311931.appspot.com",
    messagingSenderId: "166751289571"
  };
  firebase.initializeApp(config);

  // Init angular module
  beforeEach(module('app'));

  // Global vars
  var $controller = void 0,
      $compile = void 0,
      $filter = void 0,
      $httpBackend = void 0,
      $rootScope = void 0;

  // Inject services
  beforeEach(inject(function (_$controller_, _$compile_, _$filter_, _$httpBackend_, _$rootScope_) {
    $controller = _$controller_;
    $filter = _$filter_;
    $compile = _$compile_;
    $httpBackend = _$httpBackend_;
    $rootScope = _$rootScope_;
  }));

  /* ------------------------------------
    LOGIN
  ------------------------------------ */
  describe('login controller', function () {
    var $scope = void 0,
        controller = void 0;

    beforeEach(function () {
      $scope = {};
      controller = $controller('LoginController', { $scope: $scope });
    });

    it('should store State object', function () {
      expect(controller.state).toBeDefined();
    });
  });

  /* ------------------------------------
    FILTERS
  ------------------------------------ */
  describe('Filters', function () {

    describe('urlize', function () {
      it('should convert spaces to dashes', function () {
        var urlize = $filter('urlize');
        expect(urlize('text with spaces')).toBe('text-with-spaces');
      });
    });

    describe('daysAgo', function () {
      it('should return # of days ago', function () {
        var daysAgo = $filter('daysAgo');
        var selectingDate = new Date(2018, 4, 25).getTime();
        var recipeLastDate = new Date(2018, 4, 20).getTime();
        expect(daysAgo(recipeLastDate, selectingDate)).toBe(5);
      });
    });
  });

  /* ------------------------------------
    Directives
  ------------------------------------ */
  describe('Directives', function () {

    it('replaces the element with the appropriate content', function () {
      var element = $compile('<a-great-eye></a-great-eye>')($rootScope);

      $rootScope.$digest();

      expect(element.html()).toContain('lidless, wreathed in flame, 2 times');
    });
  });

  // Tryout app
  xdescribe('$scope.grade', function () {
    var $scope = void 0,
        controller = void 0;

    beforeEach(function () {
      $scope = {};
      controller = $controller('PasswordController', { $scope: $scope });
    });

    it('sets the strength to "strong" if the password length is >8 chars', function () {
      $scope.password = 'longerthaneightchars';
      $scope.grade();
      expect($scope.strength).toEqual('strong');
    });

    it('sets the strength to "weak" if the password length is <=3 chars', function () {
      $scope.password = 'bcd';
      $scope.grade();
      expect($scope.strength).toEqual('weak');
    });
  });
});
//# sourceMappingURL=services.spec.js.map
