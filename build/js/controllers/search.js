'use strict';

angular.module('app').controller("SearchController", function ($filter, $location, $log, $http, $timeout, edamam, state) {
  var vm = this;

  vm.query = '';
  vm.loadingData = false;
  vm.state = state;

  vm.search = function (ev) {
    ev.preventDefault();

    vm.loadingData = true;

    $http({
      method: 'GET',
      url: edamam.base + '/search?app_id=' + edamam.appId + '&app_key=' + edamam.appKey + '&q=' + vm.query + '&to=20'
    }).then(function (response) {
      // $log.log(response.data)
      vm.response = response.data;
      vm.loadingData = false;
    }, function (error) {
      return alert(error);
    });
  };

  vm.goBack = function () {
    vm.state.selecting.active = false;
    window.scrollTo(0, 0);
    $location.url('/recipes');
  };

  vm.seeDetail = function (searchItem, ev) {
    ev.preventDefault();
    var href = $filter('urlize')(searchItem.recipe.label),
        scrollDuration = 500;

    vm.state.recipeDetail = searchItem.recipe;

    $timeout(function () {
      $location.url('/recipe/' + href);
    }, scrollDuration);
  };
});
//# sourceMappingURL=search.js.map
