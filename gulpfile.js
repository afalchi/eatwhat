/* ----------------------------------------------------
  Common commands

  gulp dev
  gulp custom-bootstrap
  gulp copy-vendors


---------------------------------------------------- */



/* ----------------------------------------------------
  Requires
---------------------------------------------------- */
const gulp = require('gulp'),
      sass = require('gulp-sass'),
      data = require('gulp-data'),      
      stylus = require('gulp-stylus'),
      sourcemaps = require('gulp-sourcemaps'),
      babel = require('gulp-babel'),
      webserver = require('gulp-webserver'),
      concat = require('gulp-concat'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      browserSync = require('browser-sync').create(),
      ready = require("gulp-file-ready"),
      nunjucksRender = require('gulp-nunjucks-render'),
      rename = require('gulp-rename')
;
 

 
/* ----------------------------------------------------
  Nunjucks
---------------------------------------------------- */
gulp.task('nunjucks', function() {

  return gulp
    .src(['src/pages/**/*.+(html|nunjucks|njk)'])
    // Renders template with nunjucks
    .pipe(nunjucksRender({
      path: ['src'],
      envOptions: {
        tags: {
          variableStart: '{$',
          variableEnd: '$}',
        }
      } 
    }))
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('build'))
    .pipe(browserSync.stream())
  ;
});



/* ----------------------------------------------------
  Sass
---------------------------------------------------- */
gulp.task('sass', () => {
  return setTimeout( () => {
    return gulp
      .src('src/css/scss/**/*.scss')
      .pipe(sass())
      .pipe(sourcemaps.write('build'))    
      .pipe(postcss([
        autoprefixer({ browsers: ['last 2 versions'] })])
      ) 
      .pipe(gulp.dest('build'))
      .pipe(browserSync.stream())
    ;
  }, 1000); 
}); 



/* ----------------------------------------------------
  Stylus
---------------------------------------------------- */
gulp.task('stylus', () => {
  return gulp.src(['src/css/main.styl'], {base: 'src'})
    .pipe(stylus())
    .on('error', err => console.log(err))
    .pipe(sourcemaps.init()) 
    .pipe(postcss([
      autoprefixer({ browsers: ['last 3 versions'] })])
    ) 
    .pipe(sourcemaps.write('.'))  
    // .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('build'))
    .pipe(browserSync.stream())
  ;
});


/* ----------------------------------------------------
  Babel
---------------------------------------------------- */
gulp.task('babel', () => {
  return gulp
    .src('src/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({ 
      // plugins: ['transform-runtime'],
      presets: ['env'] 
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('build'))
    .pipe(browserSync.stream())
  ;
}); 



/* ----------------------------------------------------
  Custom bootstrap
---------------------------------------------------- */
gulp.task('custom-bootstrap', () => {
  return gulp.src('src/css/bootstrap/scss/**/*.scss')
    .pipe(sass())
    .pipe(sourcemaps.init())     
    .pipe(postcss([autoprefixer]))
    .pipe(sourcemaps.write()) 
    .pipe(gulp.dest('build/css/bootstrap/'))
  ;
});



/* ----------------------------------------------------
  Webserver
---------------------------------------------------- */
gulp.task('webserver', function() {
  gulp.src('./')
    .pipe(webserver({
      fallback:   'index.html',
      livereload: true,
      directoryListing: true,
      port: 8080,
      open: 'http://localhost:8080/build/index.html' 
    }));
});



/* ----------------------------------------------------
  Copy vendor files from /node_modules into /vendor
---------------------------------------------------- */
gulp.task('copy-vendors', function() {
  
  // [ node folder, /Subfolder to include]
  // [ node folder, '/' ] to include all files in the root 
  const folders = [
    ['@fancyapps', '/fancybox/dist'],
    ['angular-mocks', '/'],
    ['animejs', '/'],
    ['animate.css', '/'],
    ['bootstrap', '/dist'],
    ['font-awesome', ''],
    ['jquery', '/dist'],
    ['jquery-ui-dist', '/'],
    ['scrollreveal', '/dist'],
    ['slick-carousel', '/slick'],
    ['stickyfilljs', '/dist'],
    ['vue', '/dist'],
  ];

  
  folders.forEach( item => {
    gulp
    .src([`node_modules/${item[0] + (item[1] || '')}/**/*`])
    .pipe(gulp.dest(`build/vendor/${item[0] + (item[1] || '')}`))
    ;
  });
  
  // Per copiare solo i singoli file
  // [ folder, filename ]
  const files = [
    ['angular', 'angular.min.js'],    
    ['angular-animate', 'angular-animate.min.js'],    
    ['angular-i18n', 'angular-locale_it-it.js'],    
    ['angular-route', 'angular-route.min.js'],    
    ['angularfire', 'dist/angularfire.min.js'],    
    ['firebase', 'firebase.js'],    
    ['lodash', 'lodash.min.js'],    
    ['jquery.easing', 'jquery.easing.min.js'],    
    ['requirejs', 'require.js'],
  ];

  files.forEach( item => {
    gulp
      .src([`node_modules/${item[0]}/${item[1]}`])
      .pipe(gulp.dest(`build/vendor/${item[0]}`))
    ;
  });

});


/* ----------------------------------------------------
  Browser Sync
---------------------------------------------------- */
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: '/build'
    },
  })
}); 



/* ----------------------------------------------------
  Static Server + watching files
---------------------------------------------------- */
gulp.task('dev', ['stylus', 'babel'], function() {
  browserSync.init({
      server: "./build"
  }); 
  
  gulp.watch('src/**/*.js', ['babel']);
  gulp.watch('src/**/*.njk', ['nunjucks']);
  gulp.watch('src/css/bootstrap/**/*.scss', ['custom-bootstrap']);
  gulp.watch('src/**/*.styl', ['stylus']);
  gulp.watch("build/**/*.html").on('change', browserSync.reload);
});  