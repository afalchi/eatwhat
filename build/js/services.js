'use strict';

angular.module('app').factory('Users', function () {
  var Users = {};

  // Users.method = function() {};

  return Users;
})

/* -------------------------------------
  Check Auth
  // TODO not working as service
--------------------------------------*/
.factory('checkAuth', function ($location, state) {
  return function (currentAuth) {
    if (!currentAuth && state.loginType === undefined) {
      $location.url('/login');
      return;
    } else if (currentAuth) {
      state.user = {
        displayName: currentAuth.displayName,
        uid: currentAuth.uid
      };
    }
  };
})

/* -------------------------------------
  Firebase
--------------------------------------*/
.factory('Auth', function ($firebaseAuth) {
  return $firebaseAuth();
}).factory('recipesFB', function ($firebaseAuth, $firebaseArray, $firebaseObject) {
  return {
    auth: $firebaseAuth(),
    ref: function ref(uid) {
      return firebase.database().ref().child('eatwhat/' + uid + '/recipes');
    },
    arr: function arr(uid) {
      var ref = firebase.database().ref().child('eatwhat/' + uid + '/recipes');
      return $firebaseArray(ref);
    },
    obj: function obj(uid) {
      var ref = firebase.database().ref().child('eatwhat/' + uid + '/recipes');
      return $firebaseObject(ref);
    }
  };
})

/* -------------------------------------
  App state
--------------------------------------*/
.value('state', {
  selecting: {
    active: false,
    date: undefined,
    meal: undefined,
    selectedRecipe: undefined
  }
})

/* -------------------------------------
  Get day timestamp at midnight
--------------------------------------*/
.factory('getMidNight', function (msPer) {
  return function (d) {
    var sixHours = 1000 * 60 * 60 * 24;
    return d.getTime() - (d.getMilliseconds() + d.getSeconds() * msPer.second + d.getMinutes() * msPer.minute + d.getHours() * msPer.hour);
  };
})

/* -------------------------------------
  Get today timestamp (ms)
--------------------------------------*/
.factory('today', function (getMidNight) {
  var d = new Date();
  return getMidNight(d);
})

/* -------------------------------------
  Get last monday timestamp (ms)
--------------------------------------*/
.factory('lastMonday', function (today, msPer) {
  // Start week on Monday
  var day = function () {
    var d = new Date().getDay();
    if (d === 0) {
      return 7;
    } else {
      return d;
    }
  }();
  return today - msPer.day * (day - 1);
})

/* -------------------------------------
  Update week
--------------------------------------*/
.factory('updateWeek', function (lastMonday, msPer, recipesFB, today) {

  return function (FBArray, uid) {

    var week = [];

    function getMeal(date, meal) {
      return FBArray.find(function (item) {
        if (item.dates) {
          if (date in item.dates) {
            if (item.dates[date][meal]) {
              return item;
            }
          }
        }
      });
    }

    for (var i = 0; i < 7; i++) {
      var date = lastMonday + msPer.day * i;
      // debugger  
      week.push({
        date: date,
        lunch: getMeal(date, 'lunch'),
        dinner: getMeal(date, 'dinner')
      });
    }

    // console.log(week)
    return week;
  };
})

/* -------------------------------------
  Edamam API
--------------------------------------*/
.factory('edamam', function () {
  return {
    appId: '47bb17d8',
    appKey: '8907529822f7561d20891f11915dbce5',
    base: 'https://api.edamam.com'
  };
})

/* -------------------------------------
  Some constants
--------------------------------------*/
.constant('msPer', {
  day: 86400000,
  hour: 3600000,
  minute: 60000,
  second: 1000
}).constant('f2fKey', '85ac5ad6b603de9c05c6227f05ac33c1').constant('f2fBase', 'https://community-food2fork.p.mashape.com/');
//# sourceMappingURL=services.js.map
