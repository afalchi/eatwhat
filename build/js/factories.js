'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

angular.module('app').factory('state', function () {
  return {
    type: 'none',
    selectedRecipeId: '',
    selectedMeal: ''
  };
}).factory('recipe', function () {
  return function () {
    function Recipe(name, difficulty) {
      _classCallCheck(this, Recipe);

      this.name = name;
      this.difficulty = difficulty;
      this.lastDate = lastDate;
    }

    _createClass(Recipe, [{
      key: 'getname',
      get: function get() {
        return this.name;
      }
    }]);

    return Recipe;
  }();
}).factory('recipes', function ($firebaseArray) {
  var ref = firebase.database().ref().child('eatwhat');
  return $firebaseArray(ref);
}).factory('recipesObj', function ($firebaseObject) {
  var ref = firebase.database().ref().child('eatwhat');
  return $firebaseObject(ref);
}).factory('selectedLunch', function () {
  return '';
}).factory('todayFactory', function () {
  var d = new Date();
  return d.getTime() - d.getMilliseconds() - d.getSeconds() * 1000 - d.getMinutes() * 60000 - d.getHours() * (1000 * 60 * 24);
});
//# sourceMappingURL=factories.js.map
