'use strict';

angular.module("testing-app", [])

/* -----------------------------------
  Run method
------------------------------------*/
.run(function ($rootScope) {

  console.log('rootScope');
}).controller('PasswordController', function PasswordController($scope) {
  $scope.password = '';
  $scope.grade = function () {
    var size = $scope.password.length;
    if (size > 8) {
      $scope.strength = 'strong';
    } else if (size > 3) {
      $scope.strength = 'medium';
    } else {
      $scope.strength = 'weak';
    }
  };
});
//# sourceMappingURL=testing-app.js.map
