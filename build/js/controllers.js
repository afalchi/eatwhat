'use strict';

angular.module('app')

/* -----------------------------------
  Ricetta Food2Fork
------------------------------------*/
.controller("RecipeController", function ($log, $http, f2fBase, f2fKey, recipesFB, $routeParams) {
  var vm = this;

  recipesFB.obj.$loaded(function (data) {
    $log.log('ready');
    // vm.recipe = data;
  }, function (err) {
    return $log.log('Error fetching data!');
  });
})

/* -----------------------------------
  Ricetta Food2Fork
------------------------------------*/
.controller("F2FRecipeController", function ($log, $http, f2fBase, f2fKey, $routeParams) {
  var vm = this;

  vm.recipeId = $routeParams.recipeId;
  vm.recipe;

  $http({
    headers: {
      'Accept': 'application/json',
      'X-Mashape-Key': 'T4C7XpCx9Wmsh9ubK3pkGyYHLW7Fp1wCCFdjsnHrzWoESj0ftm'
    },
    method: 'GET',
    url: f2fBase + '/get?key=' + f2fKey + '&rId=' + $routeParams.recipeId
  }).then(function (response) {
    return vm.recipe = response.data.recipe;
  }, function (error) {
    return alert(error);
  });
});
//# sourceMappingURL=controllers.js.map
