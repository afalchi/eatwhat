// Karma configuration
// Generated on Wed May 30 2018 17:37:30 GMT+0200 (ora legale Europa occidentale)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: './',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'build/vendor/angular/angular.min.js',
      'build/vendor/angular-i18n/angular-locale_it-it.js',
      'build/vendor/angular-route/angular-route.min.js',
      'build/vendor/angular-animate/angular-animate.min.js',
      'build/vendor/firebase/firebase.js',
      'build/vendor/angularfire/angularfire.min.js',
      'build/vendor/angular-mocks/angular-mocks.js',
      'build/js/app.js',
      'build/js/controllers/main.js',
      'build/js/controllers/login.js',
      'build/js/controllers/calendar.js',
      'build/js/controllers/recipes.js',
      'build/js/controllers/search.js', 
      'build/js/controllers/recipe.js',
      'build/js/animations.js',
      'build/js/filters.js',
      'build/js/services.js',
      'build/js/components.js',
      'build/js/directives.js',
      // './build/js/testing-app.js',
      'build/js/services.spec.js'
    ],


    // list of files / patterns to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,


    // Plugins
    // plugins: [
    //   'karma-chrome-launcher',
    //   'karma-jasmine'
    // ]

  })
}
